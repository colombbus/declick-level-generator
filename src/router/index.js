import Vue from 'vue'
import Router from 'vue-router'
import MapDrawer from '@/components/MapDrawer'
import MapWriter from '@/components/MapWriter'

Vue.use(Router)

var routes = [
  {
    path: '/',
    name: 'MapDrawer',
    component: MapDrawer
  },
  {
    path: '/draw',
    name: 'MapDrawer2',
    component: MapDrawer
  },
  {
    path: '/write',
    name: 'MapWriter',
    component: MapWriter
  }
];


export default new Router({
  mode: 'history',
  routes,
})
